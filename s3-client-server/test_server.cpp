#include <gtest/gtest.h>
#include <boost/asio.hpp>
#include <vector>
#include <chrono>
#include <thread>
#include "commons.h"

TEST(Server, Shutdown) {
    ServerWrapper server(localhost, 32);
    server.Start();

    server.Shutdown();
}

TEST(Server, ShortCycle) {
    ServerWrapper server(localhost, 32);
    server.Start();

    ClientWrapper client(localhost);
    client.Sum({1});
}

TEST(Server, SumCorrectness) {
    ServerWrapper server(localhost, 32);
    server.Start();
    ClientWrapper client(localhost);
    const int iterations = 100;
    std::vector<int> values;
    for (int i = 1; i <= iterations; ++i) {
        values.push_back(i);
        auto sum = client.Sum(values);
        ASSERT_EQ(i * (i + 1) / 2, sum);
    }
}

TEST(Server, LongCycle) {
    const int clients = 4;
    const int iterations = 100;
    ServerWrapper server(localhost, clients);
    server.Start();
    std::vector<std::thread> threads;
    for (int i = 0; i < clients; ++i)
        threads.emplace_back([&server, iterations]() {
            ClientWrapper client(localhost);
            std::vector<int> values;
            for (int i = 1; i <= iterations; ++i) {
                values.push_back(i);
                auto sum = client.Sum(values);
                ASSERT_EQ(i * (i + 1) / 2, sum);
            }
        });

    for (auto& thread : threads)
        thread.join();

    server.Shutdown();
    ASSERT_EQ(clients * iterations, server.ProcessedQueryCount());
}

TEST(Server, ServerIsThreaded) {
    const int clients = 4;
    ServerWrapper server(localhost, clients);
    server.Start();

    std::thread block_thread([] () {
        asio::io_service io_service;
	tcp::socket socket(io_service);
	socket.connect(localhost);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    });

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    auto start = std::chrono::system_clock::now();
    ClientWrapper client(localhost);
    client.Sum({1});
    auto end = std::chrono::system_clock::now();
    auto passed = (std::chrono::duration_cast<std::chrono::milliseconds> (end - start)).count();

    EXPECT_LE(passed, 500);

    block_thread.join();
}


TEST(Server, MaxConnections) {
    const int clients = 4;
    ServerWrapper server(localhost, clients);
    server.Start();
    std::vector<std::thread> threads;
    for (int i = 0; i < clients; ++i) {
        threads.emplace_back([]() {
            asio::io_service io_service;
            tcp::socket socket(io_service);
            socket.connect(localhost);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        });
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    auto start = std::chrono::system_clock::now();
    ClientWrapper client(localhost);
    client.Sum({1});
    auto end = std::chrono::system_clock::now();
    auto passed = (std::chrono::duration_cast<std::chrono::milliseconds> (end - start)).count();
    EXPECT_GE(passed, 100);

    for (auto& thread : threads) thread.join();
}
